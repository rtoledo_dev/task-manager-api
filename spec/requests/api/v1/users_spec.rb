require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  let!(:user) { create(:user) }
  let(:user_id){ user.id }
  let(:headers) do
    {
      'Accept' => 'application/vnd.taskmanager.v1',
      'Content-Type' => Mime[:json].to_s,
      'Authorization' => user.auth_token
    }
  end
  before { host! "api.taskmanager.dev" }


  describe "GET /users/:id" do
    before do
      get "/users/#{user_id}", params: {}, headers: headers
    end

    context 'when user exists' do
      it 'returns the user' do
        expect(json_body['id']).to eq(user_id)
      end

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when users dont exists' do
      let(:user_id) { 10000 }

      it 'returns status 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /users' do
    before do
      post '/users', params: user_params.to_json, headers: headers
    end

    context 'valid params' do
      let(:user_params) { {user: attributes_for(:user)} }

      it 'returns status 201' do
        expect(response).to have_http_status(201)
      end

      it 'returns json user informations' do
        expect(json_body[:email]).to eq(user_params[:email])
      end

    end

    context 'invalid params' do
      let(:user_params) { {user: attributes_for(:user, email: 'teste@') } }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns json with errors' do
        expect(json_body).to have_key('errors')
      end
    end
  end

  describe 'PUT /users/:id' do
    before do
      put "/users/#{user_id}", params: user_params.to_json, headers: headers
    end

    context 'valid params' do
      let(:user_params) { {user: {email: 'agc.rodrigo@gmail.com'} } }

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns updated with new data' do
        expect(json_body['email']).to eq('agc.rodrigo@gmail.com')
      end

      it 'returns json user informations' do
        expect(json_body[:email]).to eq(user_params[:email])
      end

    end

    context 'invalid params' do
      let(:user_params) { {user: attributes_for(:user, email: 'teste123@') } }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns json with errors' do
        expect(json_body).to have_key('errors')
      end
    end
  end

  describe 'DELETE /users/:id' do
    before do
      delete "/users/#{user_id}", params: {}, headers: headers
    end

    context 'Delete users' do
      it 'returns status 204' do
        expect(response).to have_http_status(204)
      end

      it 'Expect real delete in database' do
        expect(User.find_by(id: user_id)).to eq(nil)
      end
    end
  end
end
