require 'rails_helper'

RSpec.describe 'Tasks API', type: :request do
  let!(:user) { create(:user) }
  let(:headers) do
    {
      'Accept' => 'application/vnd.taskmanager.v1',
      'Content-Type' => Mime[:json].to_s,
      'Authorization' => user.auth_token
    }
  end
  before { host! "api.taskmanager.dev" }

  describe 'GET /tasks' do
    before do
      create_list(:task, 5, user_id: user.id)
      get '/tasks', params: {}, headers: headers
    end

    it 'returns status 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns 5 tasks from database' do
      expect(json_body['tasks'].count).to eq(5)
    end
  end

  describe 'GET /tasks/:id' do
    let(:task) { create(:task, user_id: user.id )}
    before do
      get "/tasks/#{task.id}", params: {}, headers: headers
    end

    it 'returns status 200' do
      expect(response).to have_http_status(200)
    end

    it 'return the task from database' do
      expect(json_body['id']).to eq(task.id)
    end
  end

  describe 'POST /tasks' do
    before do
      post "/tasks", params: { task: task_params }.to_json, headers: headers
    end

    context 'when params have valid params' do
      let(:task_params) { attributes_for(:task) }

      it 'returns status 201' do
        expect(response).to have_http_status(201)
      end

      it 'saves the task into in the database' do
        expect(Task.find_by(title: task_params[:title])).not_to be_nil
      end

      it 'returns the task to current_user' do
        expect(json_body['title']).to eq(task_params[:title])
      end

      it 'assigns the created task to current_user' do
        expect(json_body['user_id']).to eq(user.id)
      end
    end

    context 'when params have invalid params' do
      let(:task_params) { attributes_for(:task, title: '') }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'dont saves the task into in the database' do
        expect(Task.find_by(title: task_params[:title])).to be_nil
      end

      it 'returns the json with error for title' do
        expect(json_body['errors']).to have_key('title')
      end
    end
  end

  describe 'PUT /tasks/:id' do
    let!(:task) { create(:task, user_id: user.id) }
    before do
      put "/tasks/#{task.id}", params: { task: task_params }.to_json, headers: headers
    end

    context 'when params have valid params' do
      let(:task_params) { { title: 'new title asdqwe123'} }

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'find task in database' do
        expect(Task.find_by(title: task_params[:title])).not_to be_nil
      end

      it 'returns the task to current_user' do
        expect(json_body['title']).to eq(task_params[:title])
      end

      it 'assigns the created task to current_user' do
        expect(json_body['user_id']).to eq(user.id)
      end
    end

    context 'when params have invalid params' do
      let(:task_params) { attributes_for(:task, title: '') }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'dont saves the task into in the database' do
        expect(Task.find_by(title: task_params[:title])).to be_nil
      end

      it 'returns the json with error for title' do
        expect(json_body['errors']).to have_key('title')
      end
    end
  end


  describe 'DELETE /tasks/:id' do
    let!(:task) { create(:task, user_id: user.id) }
    before do
      delete "/tasks/#{task.id}", params: {}, headers: headers
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    it 'removes the task from database' do
      expect(Task.exists?(task.id)).to be false
    end
  end
end