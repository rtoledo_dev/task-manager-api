require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  let!(:user) { create(:user) }
  let!(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.taskmanager.v2',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'client' => auth_data['client'],
      'uid' => auth_data['uid'],
    }
  end
  before { host! "api.taskmanager.dev" }


  describe "GET /auth/validate_token" do

    context 'when user exists' do
      before do
        get "/auth/validate_token", params: {}, headers: headers
      end

      it 'returns the user' do
        expect(json_body['data']['id'].to_i).to eq(user.id)
      end

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when users dont exists' do
      before do
        headers['access-token'] = 'invalid-token'
        get "/auth/validate_token", params: {}, headers: headers
      end

      it 'returns status 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'POST /auth' do
    before do
      post '/auth', params: user_params.to_json, headers: headers
    end

    context 'valid params' do
      let(:user_params) { attributes_for(:user) }

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns json user informations' do
        expect(json_body['data']['email']).to eq(user_params[:email])
      end

    end

    context 'invalid params' do
      let(:user_params) { attributes_for(:user, email: 'teste@') }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns json with errors' do
        expect(json_body).to have_key('errors')
      end
    end
  end

  describe 'PUT /auth' do
    before do
      put "/auth", params: user_params.to_json, headers: headers
    end

    context 'valid params' do
      let(:user_params) { attributes_for(:user, email: 'agc.rodrigo@gmail.com') }

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns updated with new data' do
        expect(json_body['data']['email']).to eq('agc.rodrigo@gmail.com')
      end

      it 'returns json user informations' do
        expect(json_body['data']['email']).to eq(user_params[:email])
      end

    end

    context 'invalid params' do
      let(:user_params) { {user: attributes_for(:user, email: 'teste123@') } }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns json with errors' do
        expect(json_body).to have_key('errors')
      end
    end
  end

  describe 'DELETE /delete' do
    before do
      delete "/auth", params: {}, headers: headers
    end

    context 'Delete users' do
      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'Expect real delete in database' do
        expect(User.find_by(id: user.id)).to eq(nil)
      end
    end
  end
end
