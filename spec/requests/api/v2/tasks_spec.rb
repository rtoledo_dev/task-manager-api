require 'rails_helper'

RSpec.describe 'Tasks API', type: :request do
  let!(:user) { create(:user) }
  let!(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.taskmanager.v2',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'client' => auth_data['client'],
      'uid' => auth_data['uid']
    }
  end
  before { host! "api.taskmanager.dev" }

  describe 'GET /tasks' do
    context 'when no filter is sent' do
      before do
        create_list(:task, 5, user_id: user.id)
        get '/tasks', params: {}, headers: headers
      end

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns 5 tasks from database' do
        expect(json_body['data'].count).to eq(5)
      end
    end
    
    context 'when filter and sort is sent' do
      let!(:notebook_task_1) { create(:task, title: "Check if notebook is broken", user_id: user.id) }
      let!(:notebook_task_2) { create(:task, title: "Buy new notebook", user_id: user.id) }
      let!(:notebook_task_3) { create(:task, title: "Fix door", user_id: user.id) }
      let!(:notebook_task_4) { create(:task, title: "Buy a new car", user_id: user.id) }

      before do
        get '/tasks?q[title_cont]=note&q[s]=title+ASC', params: {}, headers: headers
      end

      it 'returns only the tasks matching' do
        returned_task_titles = json_body['data'].map{|t| t['attributes']['title']}
        expect(returned_task_titles).to eq([notebook_task_2.title, notebook_task_1.title])
      end
    end
  end

  describe 'GET /tasks/:id' do
    let(:task) { create(:task, user_id: user.id )}
    before do
      get "/tasks/#{task.id}", params: {}, headers: headers
    end

    it 'returns status 200' do
      expect(response).to have_http_status(200)
    end

    it 'return the task from database' do
      expect(json_body['data']['id'].to_i).to eq(task.id)
    end
  end

  describe 'POST /tasks' do
    before do
      post "/tasks", params: { task: task_params }.to_json, headers: headers
    end

    context 'when params have valid params' do
      let(:task_params) { attributes_for(:task) }

      it 'returns status 201' do
        expect(response).to have_http_status(201)
      end

      it 'saves the task into in the database' do
        expect(Task.find_by(title: task_params[:title])).not_to be_nil
      end

      it 'returns the task to current_user' do
        expect(json_body['data']['attributes']['title']).to eq(task_params[:title])
      end

      it 'assigns the created task to current_user' do
        expect(json_body['data']['attributes']['user-id']).to eq(user.id)
      end
    end

    context 'when params have invalid params' do
      let(:task_params) { attributes_for(:task, title: '') }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'dont saves the task into in the database' do
        expect(Task.find_by(title: task_params[:title])).to be_nil
      end

      it 'returns the json with error for title' do
        expect(json_body['errors']).to have_key('title')
      end
    end
  end

  describe 'PUT /tasks/:id' do
    let!(:task) { create(:task, user_id: user.id) }
    before do
      put "/tasks/#{task.id}", params: { task: task_params }.to_json, headers: headers
    end

    context 'when params have valid params' do
      let(:task_params) { { title: 'new title asdqwe123'} }

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end

      it 'find task in database' do
        expect(Task.find_by(title: task_params[:title])).not_to be_nil
      end

      it 'returns the task to current_user' do
        expect(json_body['data']['attributes']['title']).to eq(task_params[:title])
      end

      it 'assigns the created task to current_user' do
        expect(json_body['data']['attributes']['user-id']).to eq(user.id)
      end
    end

    context 'when params have invalid params' do
      let(:task_params) { attributes_for(:task, title: '') }

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'dont saves the task into in the database' do
        expect(Task.find_by(title: task_params[:title])).to be_nil
      end

      it 'returns the json with error for title' do
        expect(json_body['errors']).to have_key('title')
      end
    end
  end


  describe 'DELETE /tasks/:id' do
    let!(:task) { create(:task, user_id: user.id) }
    before do
      delete "/tasks/#{task.id}", params: {}, headers: headers
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    it 'removes the task from database' do
      expect(Task.exists?(task.id)).to be false
    end
  end
end