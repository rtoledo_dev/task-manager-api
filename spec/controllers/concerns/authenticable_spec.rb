require 'rails_helper'

RSpec.describe Authenticable do
  # subject its the describe of the test, but in this case we change the controller for one that dont exists. Just for test
  controller(ApplicationController) do
    include Authenticable
  end

  # read the explain before
  let(:app_controller) { subject }

  describe '#current_user' do
    let(:user){ create(:user) }
    before do
      # this is usefull to dont need implement all parts for request
      req = double(headers: { 'Authorization' => user.auth_token})
      allow(app_controller).to receive(:request).and_return(req)
    end

    it 'returns the current_user from request headers' do
      expect(app_controller.current_user).to eq(user)
    end
  end

  describe '#authenticate_with_token!' do
    controller do
      before_action :authenticate_with_token!

      def restricted_action
        
      end
    end

    context 'when dont have user logged' do
      before do
        # example of mock
        allow(app_controller).to receive(:current_user).and_return(nil)

        routes.draw { get 'restricted_action' => 'anonymous#restricted_action' }

        # this action dont exists but is need for the test 
        get :restricted_action
      end

      it 'returns code 401' do
        expect(response).to have_http_status(401)
      end

      it 'returns json with errors' do
        expect(json_body).to have_key('errors')
      end
    end
  end

  describe '#user_logged_in?' do

    context 'when is user logged' do
      before do
        user = create(:user)
        allow(app_controller).to receive(:current_user).and_return(user)
      end

      it 'expect user is logged' do
        expect(app_controller.user_logged_in?).to be true
      end
    end

    context 'when is not user logged' do
      before do
        allow(app_controller).to receive(:current_user).and_return(nil)
      end

      it 'expect user is not logged' do
        expect(app_controller.user_logged_in?).to be false
      end
    end
  end
end