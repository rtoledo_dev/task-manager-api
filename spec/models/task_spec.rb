require 'rails_helper'

RSpec.describe Task, type: :model do
  context 'When user is new' do
    let(:task) { build(:task) }
    it 'task is not done' do
      expect(task).not_to be_done
    end

    it 'task belongs to user' do
      expect(task).to belong_to(:user)
    end

    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:user_id) }

    it { is_expected.to respond_to(:title) }
    it { is_expected.to respond_to(:description) }
    it { is_expected.to respond_to(:deadline) }
    it { is_expected.to respond_to(:done) }
    it { is_expected.to respond_to(:user_id) }
  end
end
